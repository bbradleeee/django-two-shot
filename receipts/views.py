from django.shortcuts import render
from django.views.generic import ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from receipts.models import ExpenseCategory, Receipt, Account

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receipts"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)
        # instances of form pluts in user for purchaser
        # super calls
        # use purchaser because that's the name for the user model attribute


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/expenselist.html"
    context_object_name = "expenses"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/expensenew.html"
    fields = ["name"]
    success_url = reverse_lazy("expense_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accountslist.html"
    context_object_name = "accounts"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/accnew.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("account_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)
